package com.gent.githubpopularrepositories.network

open class Repositories(
    val repositories: List<RepositoryJson>,
    val nextLink: String?
)