package com.gent.githubpopularrepositories.network

import io.reactivex.Single
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Url

interface GithubApi {

    @GET("/search/repositories?q=a&sort=stars")
    @Headers("Authorization: Bearer 01721261feffb35bcb3128fa0d741da0c9764871")
    fun getMostPopularRepositories(): Single<Response<RepositoriesJson>>

    @GET
    @Headers("Authorization: Bearer 01721261feffb35bcb3128fa0d741da0c9764871")
    fun getMostPopularRepositories(@Url url: String): Single<Response<RepositoriesJson>>

    @GET
    @Headers("Authorization: Bearer 01721261feffb35bcb3128fa0d741da0c9764871")
    fun getRepositoryDetails(@Url url: String): Single<RepositoryJson>

    companion object Factory {

        fun create(): GithubApi {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.github.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            return retrofit.create(GithubApi::class.java);
        }

    }
}