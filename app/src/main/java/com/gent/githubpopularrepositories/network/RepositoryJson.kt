package com.gent.githubpopularrepositories.network

import com.google.gson.annotations.SerializedName

data class RepositoryJson(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("owner") val owner: Owner,
    @SerializedName("description") val description: String,
    @SerializedName("url") val url: String,
    @SerializedName("stargazers_count") val starCount: Int,
    @SerializedName("subscribers_count") val watchersCount: Int
) {

    class Owner(
        @SerializedName("login") val login: String,
        @SerializedName("avatar_url") val avatar: String
    )
}