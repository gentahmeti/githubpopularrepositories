package com.gent.githubpopularrepositories.network

import com.gent.githubpopularrepositories.network.GithubConstants.Companion.META_FIRST
import com.gent.githubpopularrepositories.network.GithubConstants.Companion.META_LAST
import com.gent.githubpopularrepositories.network.GithubConstants.Companion.META_NEXT
import com.gent.githubpopularrepositories.network.GithubConstants.Companion.META_PREV
import com.gent.githubpopularrepositories.network.GithubConstants.Companion.META_REL

/*******************************************************************************
 *  Copyright (c) 2011 GitHub Inc.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *    Kevin Sawicki (GitHub Inc.) - initial API and implementation
 *******************************************************************************/

/**
 * Page link class to be used to determine the links to other pages of request
 * responses encoded in the current response. These will be present if the
 * result set size exceeds the per page limit.
 */
class PageLinks(linkHeader: String?) {

    /**
     * @return first
     */
    var first: String? = null
        private set
    /**
     * @return last
     */
    var last: String? = null
        private set
    /**
     * @return next
     */
    var next: String? = null
        private set
    /**
     * @return prev
     */
    var prev: String? = null
        private set

    init {
        if (linkHeader != null) {
            val links = linkHeader.split(DELIM_LINKS.toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
            for (link in links) {
                val segments = link.split(DELIM_LINK_PARAM.toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                if (segments.size < 2)
                    continue

                var linkPart = segments[0].trim({ it <= ' ' })
                if (!linkPart.startsWith("<") || !linkPart.endsWith(">"))
                //$NON-NLS-1$ //$NON-NLS-2$
                    continue
                linkPart = linkPart.substring(1, linkPart.length - 1)

                for (i in 1 until segments.size) {
                    val rel = segments[i].trim({ it <= ' ' }).split("=".toRegex()).dropLastWhile({ it.isEmpty() })
                        .toTypedArray() //$NON-NLS-1$
                    if (rel.size < 2 || !META_REL.equals(rel[0]))
                        continue

                    var relValue = rel[1]
                    if (relValue.startsWith("\"") && relValue.endsWith("\""))
                    //$NON-NLS-1$ //$NON-NLS-2$
                        relValue = relValue.substring(1, relValue.length - 1)

                    if (META_FIRST.equals(relValue))
                        first = linkPart
                    else if (META_LAST.equals(relValue))
                        last = linkPart
                    else if (META_NEXT.equals(relValue))
                        next = linkPart
                    else if (META_PREV.equals(relValue))
                        prev = linkPart
                }
            }
        }
    }

    companion object {

        private val DELIM_LINKS = "," //$NON-NLS-1$

        private val DELIM_LINK_PARAM = ";" //$NON-NLS-1$
    }
}
