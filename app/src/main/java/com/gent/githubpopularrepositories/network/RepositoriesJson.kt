package com.gent.githubpopularrepositories.network

import com.google.gson.annotations.SerializedName

data class RepositoriesJson(
    @SerializedName("total_count") val totalCount: Int,
    @SerializedName("items") val items: List<RepositoryJson>
)