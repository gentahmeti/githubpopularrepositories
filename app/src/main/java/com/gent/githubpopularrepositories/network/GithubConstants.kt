package com.gent.githubpopularrepositories.network

/*******************************************************************************
 *  Copyright (c) 2011 GitHub Inc.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *    Kevin Sawicki (GitHub Inc.) - initial API and implementation
 *******************************************************************************/

/**
 * GitHub constants
 */
interface GithubConstants {
    companion object {
        /**  */
        val META_REL = "rel" //$NON-NLS-1$
        /**  */
        val META_LAST = "last" //$NON-NLS-1$
        /**  */
        val META_NEXT = "next" //$NON-NLS-1$
        /**  */
        val META_FIRST = "first" //$NON-NLS-1$
        /**  */
        val META_PREV = "prev" //$NON-NLS-1$
    }
}
