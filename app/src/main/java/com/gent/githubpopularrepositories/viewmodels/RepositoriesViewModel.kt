package com.gent.githubpopularrepositories.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gent.githubpopularrepositories.models.Repository
import com.gent.githubpopularrepositories.network.Repositories
import com.gent.githubpopularrepositories.repositories.GithubRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RepositoriesViewModel(private val repository: GithubRepository) : ViewModel() {

    private val disposables = CompositeDisposable()
    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>>
        get() = _repositories

    var nextLink: String? = null
    var loading = false

    init {
        disposables.add(
            repository.getMostPopularRepositories()
                .compose(::handleResponseParsing)
                .subscribe { repositories -> _repositories.postValue(repositories) }
        )
    }

    fun getNextPage() {
        if (nextLink != null && !loading) {
            loading = true
            disposables.add(
                repository.getNextPageOfMostPopularRepositories(nextLink!!)
                    .compose(::handleResponseParsing)
                    .subscribe { repositories -> _repositories.value = repositories }
            )
        }
    }

    private fun handleResponseParsing(stream: Single<Repositories>) =
        stream.doOnSuccess { nextLink = it.nextLink }
            .map { it.repositories }
            .flatMapObservable { Observable.fromIterable(it) }
            .map {
                Repository(
                    it.owner.login,
                    it.owner.avatar,
                    it.name,
                    it.description,
                    it.starCount,
                    it.watchersCount,
                    it.url
                )
            }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { loading = false }

    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}