package com.gent.githubpopularrepositories.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.gent.githubpopularrepositories.repositories.GithubRepository

class ViewModelFactory(private val repository: GithubRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(RepositoriesViewModel::class.java)) {
            RepositoriesViewModel(this.repository) as T
        } else if (modelClass.isAssignableFrom(RepositoryDetailsViewModel::class.java)) {
            RepositoryDetailsViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}