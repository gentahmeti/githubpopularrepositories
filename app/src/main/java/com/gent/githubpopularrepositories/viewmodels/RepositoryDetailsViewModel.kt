package com.gent.githubpopularrepositories.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gent.githubpopularrepositories.models.Repository
import com.gent.githubpopularrepositories.repositories.GithubRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class RepositoryDetailsViewModel(private val repository: GithubRepository) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val _repository = MutableLiveData<Repository>()
    val repositoryLiveData: LiveData<Repository>
        get() = _repository

    fun getRepositoryDetails(url: String) {
        disposables.add(
            Observable.interval(0, 1000, TimeUnit.MILLISECONDS)
                .flatMapSingle { repository.getRepositoryDetails(url) }
                .map {
                    Repository(
                        it.owner.login,
                        it.owner.avatar,
                        it.name,
                        it.description,
                        it.starCount,
                        it.watchersCount,
                        it.url
                    )
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _repository.value = it }
        )
    }

    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}