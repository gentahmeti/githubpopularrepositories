package com.gent.githubpopularrepositories.repositories

import com.gent.githubpopularrepositories.network.GithubApi
import com.gent.githubpopularrepositories.network.PageLinks
import com.gent.githubpopularrepositories.network.Repositories
import com.gent.githubpopularrepositories.network.RepositoryJson
import io.reactivex.Single

class NetworkRepository(private val githubApi: GithubApi) : GithubRepository {
    override fun getRepositoryDetails(url: String): Single<RepositoryJson> =
        githubApi.getRepositoryDetails(url)

    override fun getMostPopularRepositories(): Single<Repositories> =
        githubApi.getMostPopularRepositories()
            .map {
                Repositories(
                    it.body()!!.items,
                    PageLinks(it.headers()["link"]).next
                )
            }

    override fun getNextPageOfMostPopularRepositories(url: String): Single<Repositories> =
        githubApi.getMostPopularRepositories(url)
            .map {
                Repositories(
                    it.body()!!.items,
                    PageLinks(it.headers()["link"]).next
                )
            }
}