package com.gent.githubpopularrepositories.repositories

import com.gent.githubpopularrepositories.network.Repositories
import com.gent.githubpopularrepositories.network.RepositoryJson
import io.reactivex.Single

interface GithubRepository {

    fun getRepositoryDetails(url: String): Single<RepositoryJson>

    fun getMostPopularRepositories(): Single<Repositories>

    fun getNextPageOfMostPopularRepositories(url: String): Single<Repositories>
}