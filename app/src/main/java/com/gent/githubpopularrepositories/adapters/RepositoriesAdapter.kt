package com.gent.githubpopularrepositories.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gent.githubpopularrepositories.R
import com.gent.githubpopularrepositories.models.Repository
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class RepositoriesAdapter(
    private val picasso: Picasso
) : RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {

    private val repositories = mutableListOf<Repository>()
    private val itemClickSubject = PublishSubject.create<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            picasso,
            layoutInflater.inflate(R.layout.view_repository_item, parent, false),
            itemClickSubject
        )
    }

    override fun getItemCount() = repositories.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) = viewHolder.bindView(repositories[position])

    fun addRepositories(repositories: List<Repository>) {
        val insertionPosition = this.repositories.size
        this.repositories.addAll(repositories)
        notifyItemRangeInserted(insertionPosition, repositories.size)
    }

    fun onItemClickObservable(): Observable<Repository> =
        itemClickSubject.map { repositories[it] }

    class ViewHolder(
        private val picasso: Picasso,
        itemView: View,
        itemClick: Subject<Int>
    ) : RecyclerView.ViewHolder(itemView) {

        private val ownerAvatar: ImageView = itemView.findViewById(R.id.ownerAvatar)
        private val repoName: TextView = itemView.findViewById(R.id.repositoryName)

        init {
            itemView.setOnClickListener { itemClick.onNext(adapterPosition) }
        }

        fun bindView(repository: Repository) {
            picasso.load(repository.ownerAvatar)
                .into(ownerAvatar)
            repoName.text = repository.repoName
        }
    }
}