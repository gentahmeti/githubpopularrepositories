package com.gent.githubpopularrepositories.models

data class Repository(
    val ownerName: String,
    val ownerAvatar: String,
    val repoName: String,
    val repoDescription: String,
    val starCount: Int,
    val watcherCount: Int,
    val repoUrl: String
)