package com.gent.githubpopularrepositories

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.gent.githubpopularrepositories.adapters.RepositoriesAdapter
import com.gent.githubpopularrepositories.models.Repository
import com.gent.githubpopularrepositories.network.GithubApi
import com.gent.githubpopularrepositories.repositories.NetworkRepository
import com.gent.githubpopularrepositories.viewmodels.RepositoriesViewModel
import com.gent.githubpopularrepositories.viewmodels.ViewModelFactory
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import com.squareup.picasso.Picasso
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val repositoriesAdapter = RepositoriesAdapter(Picasso.get())
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repositoriesList.adapter = repositoriesAdapter

        val layoutManager = LinearLayoutManager(this)
        repositoriesList.layoutManager = layoutManager

        val model = ViewModelProviders.of(this, ViewModelFactory(NetworkRepository(GithubApi.create())))
            .get(RepositoriesViewModel::class.java)

        model.repositories.observe(this, Observer<List<Repository>> {
            repositoriesAdapter.addRepositories(it!!)
        })


        disposables.addAll(
            repositoriesAdapter.onItemClickObservable()
                .subscribe(::goToRepositoryDetail),

            RxRecyclerView.scrollEvents(repositoriesList)
                .filter {
                    val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                    val totalItemCount = layoutManager.itemCount
                    val visibleThreshold = 5

                    (lastVisiblePosition + visibleThreshold) > totalItemCount
                }
                .subscribe {
                    model.getNextPage()
                }
        )
    }

    private fun goToRepositoryDetail(repository: Repository) {
        val intent = Intent(this, RepositoryDetailActivity::class.java)
        intent.putExtra(RepositoryDetailActivity.EXTRA_REPO_URL, repository.repoUrl)
        startActivity(intent)
    }
}
