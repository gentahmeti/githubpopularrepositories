package com.gent.githubpopularrepositories

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gent.githubpopularrepositories.models.Repository
import com.gent.githubpopularrepositories.network.GithubApi
import com.gent.githubpopularrepositories.repositories.NetworkRepository
import com.gent.githubpopularrepositories.viewmodels.RepositoryDetailsViewModel
import com.gent.githubpopularrepositories.viewmodels.ViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_repository_detail.*

class RepositoryDetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_REPO_URL = "REPO_URL"
    }

    private val picasso = Picasso.get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_repository_detail)

        val model = ViewModelProviders.of(this, ViewModelFactory(NetworkRepository(GithubApi.create())))
            .get(RepositoryDetailsViewModel::class.java)

        model.repositoryLiveData.observe(this, Observer<Repository> {
            bindRepository(it!!)
        })

        if (savedInstanceState == null) {
            model.getRepositoryDetails(intent.extras?.getString(EXTRA_REPO_URL) ?: "")
        }
    }

    private fun bindRepository(repository: Repository) {
        picasso.load(repository.ownerAvatar)
            .into(ownerAvatar)

        repositoryName.text = repository.repoName
        ownerName.text = repository.ownerName
        watchersCount.text = repository.watcherCount.toString()
        starsCount.text = repository.starCount.toString()
        repoDescription.text = repository.repoDescription
    }
}