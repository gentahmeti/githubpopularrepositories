### Summary

This project connects to the GitHub API v3 and shows a list of all public repositories sorted by stars in descending order,
so the most popular first.
You can also click on any of the listed repositories to see some extra information about said repository.

### How to build

In order to build the app you must first generate a token on GitHub, use the following link for details on how to do that:

https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/

You only need the `public_repo` scope. Once you get a token, make sure to save it somewhere.
On the file `GithubApi.kt` replace the lines reading `Authorization: Bearer *some token*` with the token you received from GitHub.

After doing that you can build the app normally, through the command line or Android Studio or any other IDE which
supports Android development. 

For ease of use I have used a personal token which I generated.

### Architecture and patterns

For this project I'm using the MVVM architecture with reactive extensions. I'm using MVVM because of the clear
separation of the ViewModel and View, we only expose an Observable of the model and it doesn't matter who's
subscribing on the other hand.

I'm also using the Repository pattern to abstract how we get the data. I'm only using network calls for now,
but it can easily be changed to cache data and use that before making a new request.

One thing I would have liked to add are Use cases. We can use UseCases to link View models with Repositories.
This way the View model will only be exposed to a subset of the Repositories functionality.
I would usually use view states, to indicate errors, progress and success with live data. But this time I omitted error handling
and progress indicators.

I'm not using any DI library, like Dagger2, because we have only a few classes which would benefit from it, and
the overhead of adding Dagger2 isn't worth it. But I'm using manual DI, by providing dependencies to view models and repositories
through their constructors.

### Testing

Unfortunately I only have basic knowledge of the Android Architecture Components, as I have only used them for toy projects
and to get more familiar with it. For this reason I couldn't get around the caveats of testing View Models and RxJava together,
I ran into a few problems which I couldn't solve in time.